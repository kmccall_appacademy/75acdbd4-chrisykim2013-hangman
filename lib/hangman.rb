class Hangman
  attr_reader :guesser, :referee, :board, :guess, :response
  def initialize(players)
    @players = players
    @guesser, @referee = @players[:guesser], @players[:referee]
    @tries = 0
  end

  def setup
    secret_length = @referee.pick_secret_word
    @guesser.register_secret_length(secret_length)
    @board = Array.new(secret_length)
  end

  def take_turn
    @guess = @guesser.guess(@board)
    @response = @referee.check_guess(@guess)
    update_board
    @guesser.handle_response(@guess, @response)
  end

  def update_board
    @response.each {|i| @board[i] = @guess}
    @tries += 1 if @response.empty?
    render_man
  end

  def play
    setup
    until won? || @tries == 6
      take_turn
    end
    puts won? ? "Guesser wins!" : "Referee wins!"
  end

  def won?
    !@board.include?(nil)
  end

  def render_man
    case @tries
    when 0
      puts "    ____"
      puts "   |    |"
      puts "        |"
      puts "        |"
      puts "        |"
      puts "   _____|"
    when 1
      puts "    ____"
      puts "   |    |"
      puts "   O    |"
      puts "        |"
      puts "        |"
      puts "   _____|"
    when 2
      puts "    ____"
      puts "   |    |"
      puts "   O    |"
      puts "   |    |"
      puts "        |"
      puts "    ____|"
    when 3
      puts "    ____"
      puts "   |    |"
      puts "   O    |"
      puts "   |\\   |"
      puts "        |"
      puts "    ____|"
    when 4
      puts "    ____"
      puts "   |    |"
      puts "   O    |"
      puts "  /|\\   |"
      puts "        |"
      puts "    ____|"
    when 5
      puts "    ____"
      puts "   |    |"
      puts "   O    |"
      puts "  /|\\   |"
      puts "    \\   |"
      puts "    ____|"
    when 6
      puts "    ____"
      puts "   |    |"
      puts "   O    |"
      puts "  /|\\   |"
      puts "  / \\   |"
      puts "    ____|"
    end
  end
end

class HumanPlayer
  def initialize
    @history = []
  end

  def pick_secret_word
    puts "Referee, think of a word. How many characters does the word have?"
    @word_length = gets.chomp.to_i
  end

  def register_secret_length(length)
    puts "Guesser, the length of the secret word is #{length}."
  end

  def guess(board)
    render(board)
    puts "Guesser, please guess a letter you have not already used!"
    letter = gets.chomp
    until !@history.include?(letter) && letter.length == 1
      puts "You already chose this letter or used multiple. Please provide another."
      letter = gets.chomp
    end
    @history << letter
    letter
  end

  def render(board)
    puts board.map {|ch| ch.nil? ? "_" : "#{ch}"}.join(" ")
  end

  def check_guess(ch)
    idx = []
    puts "Referee, the guess was \"#{ch}\", please provide all indices where the"
    puts "letter occurs! (0123...etc.) or click enter if there is no match."
    indices = gets.chomp
    indices.chars.each {|i| idx << i.to_i}
    idx
  end

  def handle_response(ch, response)
    puts response.empty? ? "Sorry, you had no matches." : "You got a match(es)!"
    puts "Indices for \"#{ch}\" are as followed: #{response}"
  end
end

class ComputerPlayer
  attr_accessor :word
  def initialize(dictionary = File.readlines("dictionary.txt"))
    @dictionary, @history = dictionary, []
  end

  def pick_secret_word
    @word = @dictionary.sample.delete("\n")
    @word.length
  end

  def register_secret_length(length)
    @dictionary.reject! {|word| word.delete("\n").length != length}
  end

  def guess(board)
    render(board)
    @history += board.compact.uniq
    letter_count = Hash.new(0)
    @dictionary.each do |word|
      word.chars.each {|ch| letter_count[ch] += 1 unless @history.include?(ch) || ch == "\n"}
    end
    guess = letter_count.sort_by {|k, v| v}[-1][0]
    @history << guess
    guess
  end

  def check_guess(ch)
    idx = []
    @word.chars.each.with_index {|c, i| idx << i if c == ch}
    idx
  end

  def handle_response(ch, idc)
    @dictionary.reject! {|word| !met_condition?(word, ch, idc)}
  end

  def met_condition?(word, ch, idc)
    return false if idc.empty? && word.include?(ch)
    return false if word.count(ch) > idc.length
    idc.each {|i| return false if word[i] != ch}
    true
  end

  def candidate_words
    @dictionary
  end

  def render(board)
    puts board.map {|ch| ch.nil? ? "_" : "#{ch}"}.join(" ")
  end
end

if __FILE__ == $PROGRAM_NAME
  puts "Welcome to Hangman! Please choose an option (referee v guesser):"
  puts "pvp, pvc, cvp"
  ans = gets.chomp
  until ["pvp", "pvc", "cvp"].include?(ans)
    puts "Invalid response. Please choose pvp, pvc, or cvp."
    ans = gets.chomp
  end
  case ans
  when "pvp"
    player1 = HumanPlayer.new
    player2 = HumanPlayer.new
  when "pvc"
    player1 = ComputerPlayer.new
    player2 = HumanPlayer.new
  when "cvp"
    player1 = HumanPlayer.new
    player2 = ComputerPlayer.new
  end
  hash = {guesser: player1, referee: player2}
  game = Hangman.new(hash)
  game.play
end
